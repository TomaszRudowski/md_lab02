package no.ntnu.imt3673.tomaszmr.lab_02.database;

import android.arch.persistence.room.Database;
import android.arch.persistence.room.Room;
import android.arch.persistence.room.RoomDatabase;
import android.content.Context;

/**
 * Created by Reyn on 01-Mar-18.
 */

@Database(entities = {EntryEntity.class}, version = 1)
public abstract class AppDatabase extends RoomDatabase {

    public abstract EntryEntityDAO entryEntityDAO();

    // singelton from https://medium.com/@magdamiu/android-room-persistence-library-97ad0d25668e
    private static AppDatabase INSTANCE;

    public static AppDatabase getAppDatabase(Context context) {
        if (INSTANCE == null) {
            INSTANCE =
                    Room.databaseBuilder(context.getApplicationContext(), AppDatabase.class, "entries-database")
                         /*   // allow queries on the main thread. //disabled for testing
                            // Don't do this on a real app! See PersistenceBasicSample for an example.
                            .allowMainThreadQueries()
                           */
                            .build();
        }
        return INSTANCE;
    }

    public static void destroyInstance() {
        INSTANCE = null;
    }

}
