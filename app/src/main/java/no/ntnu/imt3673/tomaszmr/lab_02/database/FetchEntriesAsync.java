package no.ntnu.imt3673.tomaszmr.lab_02.database;

import android.content.Context;
import android.os.AsyncTask;

import java.util.List;

import no.ntnu.imt3673.tomaszmr.lab_02.EntriesListViewAdapter;

/**
 * Created by Reyn on 08-Mar-18.
 */

public class FetchEntriesAsync extends AsyncTask <EntriesListViewAdapter, Void, List<EntryEntity> > {

    private Context appContext;
    private EntriesListViewAdapter adapter;

    // need to pass application context from service accessing db
    public FetchEntriesAsync(Context appContext) {
        this.appContext = appContext;
    }

   /* @Override
    protected List<EntryEntity> doInBackground(Void... voids) {
        List<EntryEntity> lastEntries = AppDatabase.getAppDatabase(this.appContext).entryEntityDAO().getAll();
        return lastEntries;
    }*/

    @Override
    protected List<EntryEntity> doInBackground(EntriesListViewAdapter... entriesListViewAdapters) {
        this.adapter = entriesListViewAdapters[0];
        List<EntryEntity> lastEntries = AppDatabase.getAppDatabase(this.appContext).entryEntityDAO().getAll();
        return lastEntries;
    }

    @Override
    protected void onPostExecute(List<EntryEntity> lastEntries) {
        adapter.updateLastEntries(lastEntries);
    }
}
