package no.ntnu.imt3673.tomaszmr.lab_02;

import android.app.Service;
import android.app.job.JobParameters;
import android.app.job.JobService;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.IBinder;
import android.preference.PreferenceManager;

import org.xmlpull.v1.XmlPullParserException;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.List;

import no.ntnu.imt3673.tomaszmr.lab_02.database.EntryEntity;
import no.ntnu.imt3673.tomaszmr.lab_02.database.FetchEntriesAsync;
import no.ntnu.imt3673.tomaszmr.lab_02.database.ReplaceEntriesAsync;
import no.ntnu.imt3673.tomaszmr.lab_02.parser.RssXmlParser;

public class UpdateRssFeedService extends JobService {

    private boolean viewReloadRequired;

    public UpdateRssFeedService() {
    }

    @Override
    public boolean onStartJob(JobParameters jobParameters) {
        System.out.println(".................onStartJob running...");
        SharedPreferences sharedPrefs = PreferenceManager.getDefaultSharedPreferences(this);
        String prefURL = sharedPrefs.getString(MainActivity.PREF_RSS_URL, "");
        this.viewReloadRequired = false;
        new DownloadXmlTask().execute(prefURL);
        return false;
    }

    @Override
    public boolean onStopJob(JobParameters jobParameters) {
        System.out.println("....................onStopJob running...");
        return false;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        System.out.println(".........................onStartCommand running...");
            this.viewReloadRequired = intent.getBooleanExtra("ReloadView", false);
            // gets URL and number of items from SharedPreferences
            SharedPreferences sharedPrefs = PreferenceManager.getDefaultSharedPreferences(this);
            String prefURL = sharedPrefs.getString(MainActivity.PREF_RSS_URL, "");
            int prefNoOfItems = sharedPrefs.getInt(MainActivity.PREF_NO_OF_ITEMS, MainActivity.DEFAULT_NO_OF_ITEMS);
            //loadXmlFromNetwork();
            new DownloadXmlTask().execute(prefURL);

        return START_STICKY;
    }


    // Implementation of AsyncTask used to download XML feed from stackoverflow.com.
    private class DownloadXmlTask extends AsyncTask<String, Void, String> {
        @Override
        protected String doInBackground(String... urls) {
            System.out.println("....................downloadXmlTask running...");
            try {
                return loadXmlFromNetwork(urls[0]);
            } catch (IOException e) {
                return getResources().getString(R.string.connection_error);
            } catch (XmlPullParserException e) {
                return getResources().getString(R.string.xml_error);
            }
        }

        @Override
        protected void onPostExecute(String result) {
            System.out.println("....................onPostExecute running...");
        /*    setContentView(R.layout.main);
            // Displays the HTML string in the UI via a WebView
            WebView myWebView = (WebView) findViewById(R.id.webview);
            myWebView.loadData(result, "text/html", null);*/
        }

        /**
         * Based on: https://developer.android.com/training/basics/network-ops/xml.htm
         * Uploads XML from url found in SharedPreferences, parses it and updates ListView
         * @return
         * @throws XmlPullParserException
         * @throws IOException
         */
        private String loadXmlFromNetwork(String rssURL) throws XmlPullParserException, IOException {
            InputStream stream = null;
            // Instantiate the parser
            RssXmlParser rssXmlParser = new RssXmlParser();
            List<EntryEntity> entries = null;
            String title = null;
            String url = null;



            if (rssURL == null || rssURL.isEmpty()) {
                System.out.println("Empty URL");
            } else {
                try {
                    stream = downloadUrl(rssURL);
                    entries = rssXmlParser.parse(stream);
                    // TODO: update ListView with entries (use prefNoOfItems) ?
                    // TODO: save entries to db

                    if (viewReloadRequired) {
                        new ReplaceEntriesAsync(getApplicationContext()){
                            protected void onPostExecute(Void aVoid){
                                super.onPostExecute(aVoid);
                                new FetchEntriesAsync(getApplicationContext()).execute(MainActivity.getAdapter());
                            }
                        }.execute(entries);
                    } else {
                        new ReplaceEntriesAsync(getApplicationContext()).execute(entries);
                    }

                    // Makes sure that the InputStream is closed after the app is
                    // finished using it.
                } finally {
                    if (stream != null) {
                        stream.close();
                    }
                }
            }
            return "Done";
        }

        //

        /**
         * From : https://developer.android.com/training/basics/network-ops/xml.htm
         * Given a string representation of a URL, sets up a connection and gets an input stream.
         * @param urlString
         * @return
         * @throws IOException
         */
        private InputStream downloadUrl(String urlString) throws IOException {
            URL url = new URL(urlString);
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setReadTimeout(10000 /* milliseconds */);
            conn.setConnectTimeout(15000 /* milliseconds */);
            conn.setRequestMethod("GET");
            conn.setDoInput(true);
            // Starts the query
            conn.connect();
            return conn.getInputStream();
        }
    }


    /**
     * Based on: https://developer.android.com/training/basics/network-ops/xml.htm
     * Uploads XML from url found in SharedPreferences, parses it and updates ListView
     * @return
     * @throws XmlPullParserException
     * @throws IOException
     *
    private void loadXmlFromNetwork() throws XmlPullParserException, IOException {
        InputStream stream = null;
        // Instantiate the parser
        RssXmlParser rssXmlParser = new RssXmlParser();
        List<RssXmlParser.Entry> entries = null;
        String title = null;
        String url = null;

        // gets URL and number of items from SharedPreferences
        SharedPreferences sharedPrefs = PreferenceManager.getDefaultSharedPreferences(this);
        String prefURL = sharedPrefs.getString(MainActivity.PREF_RSS_URL, "");
        int prefNoOfItems = sharedPrefs.getInt(MainActivity.PREF_NO_OF_ITEMS, MainActivity.DEFAULT_NO_OF_ITEMS);

        if (prefURL == null || prefURL.isEmpty()) {
            System.out.println("Empty URL");
        } else {
            try {
                stream = downloadUrl(prefURL);
                entries = rssXmlParser.parse(stream);
                // TODO: update ListView with entries (use prefNoOfItems)

                for (RssXmlParser.Entry entry : entries) {
                    System.out.println(entry);
                }

                // Makes sure that the InputStream is closed after the app is
                // finished using it.
            } finally {
                if (stream != null) {
                    stream.close();
                }
            }
        }
    }

    //

    /**
     * From : https://developer.android.com/training/basics/network-ops/xml.htm
     * Given a string representation of a URL, sets up a connection and gets an input stream.
     * @param urlString
     * @return
     * @throws IOException
     *
    private InputStream downloadUrl(String urlString) throws IOException {
        URL url = new URL(urlString);
        HttpURLConnection conn = (HttpURLConnection) url.openConnection();
        conn.setReadTimeout(10000);
        conn.setConnectTimeout(15000);
        conn.setRequestMethod("GET");
        conn.setDoInput(true);
        // Starts the query
        conn.connect();
        return conn.getInputStream();
    }
*/
}
