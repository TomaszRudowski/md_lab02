package no.ntnu.imt3673.tomaszmr.lab_02;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.webkit.WebView;

public class ContentActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_content);

        WebView contentView = findViewById(R.id.wv_content);
        contentView.loadUrl(getIntent().getStringExtra("url"));
    }
}
