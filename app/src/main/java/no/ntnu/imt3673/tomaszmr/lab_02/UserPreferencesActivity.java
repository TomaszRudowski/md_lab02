package no.ntnu.imt3673.tomaszmr.lab_02;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

public class UserPreferencesActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_preferences);

        setupInitValues();
    }

    /**
     * Confirmation of current input (RSS url, Number of Items)
     * send to MainActivity as a result and finish
     * @param view
     */
    public void confirmBtnOnClick (View view) {
        Intent intent = new Intent();

        // TODO: if NoOfItems changed fetch more/less entries from db
        // TODO: alternative, additional forced RSS update call

        intent.putExtra("newRssUrl", getInputRssUrl());
        intent.putExtra("newNoOfItems", getInputNoOfItems());
        intent.putExtra("newFrequency", getFrequency());

        setResult(Activity.RESULT_OK, intent);
        finish();
        //checkInputValuesToast();
    }

    /**
     * get input text as a String
     * @return RSS URL from user input
     */
    private String getInputRssUrl() {
        TextView rssUrlInput = (TextView) findViewById(R.id.rssUrlText);
        return rssUrlInput.getText().toString();
    }

    /**
     * get a text content of a radio button parsed as an int
     * @return int selected number of items
     */
    private int getInputNoOfItems () {
        RadioGroup noOfItems = (RadioGroup) findViewById(R.id.rg_no_of_items);
        int selectedRadioBtn = noOfItems.getCheckedRadioButtonId();
        return Integer.parseInt(((RadioButton) findViewById(selectedRadioBtn)).getText().toString());
    }

    /**
     * get a text content of a radio button parsed as int
     * @return int frquency in minutes
     */
    private int getFrequency () {
        RadioGroup frequency = (RadioGroup) findViewById(R.id.rg_frequency);
        int selectedRadioBtn = frequency.getCheckedRadioButtonId();
        int valueInMinutes;
        switch (selectedRadioBtn) {
            case R.id.rb_freq_15: valueInMinutes = 15; break;
            case R.id.rb_freq_30: valueInMinutes = 30;; break;
            case R.id.rb_freq_day: valueInMinutes = 1440;; break;
            default: valueInMinutes = 15;;
        }
        return valueInMinutes;
    }

    /**
     * check correct radio button and set input text to current preferences
     */
    private void setupInitValues () {
        String rssUrlPref = readStringPreferences(MainActivity.PREF_RSS_URL);

        TextView rssUrlInput = (TextView) findViewById(R.id.rssUrlText);
        rssUrlInput.setText(rssUrlPref);

        int noOfItems = readIntPreferences(MainActivity.PREF_NO_OF_ITEMS);

        RadioGroup noOfItemSettings = (RadioGroup) findViewById(R.id.rg_no_of_items);
        int radioBtnId;
        switch (noOfItems) {
            case 10: radioBtnId = R.id.radioButton10; break;
            case 20: radioBtnId = R.id.radioButton20; break;
            case 50: radioBtnId = R.id.radioButton50; break;
            case 100: radioBtnId = R.id.radioButton100; break;
            default: radioBtnId = R.id.radioButton10; break;
        }
        noOfItemSettings.check(radioBtnId);

        RadioGroup frequency = (RadioGroup) findViewById(R.id.rg_frequency);
        int frequencyRadioBtnId;
        switch (readIntPreferences(MainActivity.PREF_FREQUENCY)) {
            case 10: frequencyRadioBtnId = R.id.rb_freq_15; break;
            case 30: frequencyRadioBtnId = R.id.rb_freq_30; break;
            case 1440: frequencyRadioBtnId = R.id.rb_freq_day; break;
            default: frequencyRadioBtnId = R.id.rb_freq_15;
        }
        frequency.check(frequencyRadioBtnId);

        checkInputValuesToast();
    }

    /**
     * read preferences as String (from imt3673 wiki)
     * @param key
     * @return
    */
    private String readStringPreferences(String key) {
        final SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
        final String value = prefs.getString(key, "");  // default value "" if preferences not found
        return value;
    }

    /**
     * read preferences as Int
     * @param key
     * @return
     */
    private int readIntPreferences(String key) {
        final SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
        final int pos = prefs.getInt(key, -1);  // default value -1 if preferences not found
        return pos;
    }

    /**
     * shows current preferences and input values
     */
    private void checkInputValuesToast() {
        Context context = getApplicationContext();
        String text = "Pref: url(" + readStringPreferences(MainActivity.PREF_RSS_URL) +
                "), noOfItems(" + readIntPreferences(MainActivity.PREF_NO_OF_ITEMS) + ")" +
                "\nInput: url(" + getInputRssUrl() + "), noOfItems(" +
                getInputNoOfItems() + ")";
        int duration = Toast.LENGTH_SHORT;

        Toast toast = Toast.makeText(context, text, duration);
        toast.show();
    }
}
