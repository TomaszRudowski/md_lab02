package no.ntnu.imt3673.tomaszmr.lab_02.database;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.support.annotation.NonNull;

/**
 * Created by Reyn on 01-Mar-18.
 */

@Entity(
        tableName = "entries",
        primaryKeys = {"title", "link"}
)
public class EntryEntity {
    /*@PrimaryKey
    private int uid;*/
    @NonNull
    @ColumnInfo(name = "title")
    private String title;

    @NonNull
    @ColumnInfo(name = "link")
    private String link;

    public EntryEntity(String title, String link) {
        this.title = title;
        this.link = link;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }

    // need only title to populate ListView, keeping default toString functionality of ArrayAdapter
    @Override
    public String toString() {
        return this.title;
    }
}
