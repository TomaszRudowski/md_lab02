package no.ntnu.imt3673.tomaszmr.lab_02.database;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;

import java.util.List;

/**
 * Created by Reyn on 01-Mar-18.
 */

@Dao
public interface EntryEntityDAO {
    @Query("SELECT * FROM entries")
    List<EntryEntity> getAll();

   /* @Query("SELECT * FROM entries WHERE uid IN (:userIds)")
    List<EntryEntity> loadAllByIds(int[] userIds);*/

    @Insert
    void insertAll(List<EntryEntity> entries);

    @Insert
    void insert(EntryEntity entry);

    @Delete
    void delete(EntryEntity entry);

    @Query("DELETE FROM entries")
    void delete();


}
