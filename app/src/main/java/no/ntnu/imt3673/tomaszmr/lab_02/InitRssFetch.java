package no.ntnu.imt3673.tomaszmr.lab_02;

import android.app.job.JobParameters;
import android.app.job.JobService;

/**
 * Created by Reyn on 15-Mar-18.
 */

public class InitRssFetch extends JobService {
    @Override
    public boolean onStartJob(JobParameters jobParameters) {
        System.out.println(".................onStartJob running...");
        doSampleJob(jobParameters);
        //jobFinished(jobParameters, false);
        return true;
    }

    @Override
    public boolean onStopJob(JobParameters jobParameters) {
        System.out.println(".................onStopJob running...");
        return false;
    }

    public void doSampleJob(JobParameters params) {
        System.out.println(".................sampleJob running...");
        // At the end inform job manager the status of the job.
        jobFinished(params, false);
    }
}
