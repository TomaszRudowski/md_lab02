package no.ntnu.imt3673.tomaszmr.lab_02.database;

import android.content.Context;
import android.os.AsyncTask;

import java.sql.Time;
import java.sql.Timestamp;
import java.util.List;

import no.ntnu.imt3673.tomaszmr.lab_02.MainActivity;

public class ReplaceEntriesAsync extends AsyncTask<List<EntryEntity>, Void, Void> {

    private Context appContext;

    // need to pass application context from service accessing db
    public ReplaceEntriesAsync(Context appContext) {
        this.appContext = appContext;
    }

    @Override
    protected Void doInBackground(List<EntryEntity>[] lists) {
        AppDatabase.getAppDatabase(this.appContext).entryEntityDAO().delete();
        AppDatabase.getAppDatabase(this.appContext).entryEntityDAO().insertAll(lists[0]);
        return null;
    }

    @Override
    protected void onPostExecute(Void aVoid) {
        super.onPostExecute(aVoid);
    }
}
