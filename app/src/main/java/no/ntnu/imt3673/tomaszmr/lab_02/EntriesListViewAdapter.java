package no.ntnu.imt3673.tomaszmr.lab_02;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.widget.ArrayAdapter;

import java.util.List;

import no.ntnu.imt3673.tomaszmr.lab_02.database.EntryEntity;

/**
 * Created by Reyn on 08-Mar-18.
 */

public class EntriesListViewAdapter extends ArrayAdapter<EntryEntity> {

    public void updateLastEntries(List<EntryEntity> entries) {
        /*System.out.println("__current entries:__");
        for (EntryEntity entry : entries) {
            System.out.println(entry);
        }*/
        int noOfItemsPref = readIntPreferences("noOfItems");
        int currentListSize = entries.size();
        this.clear();
        this.addAll(entries.subList(0, (noOfItemsPref > currentListSize) ? currentListSize : noOfItemsPref));
        //this.addAll(entries);
        notifyDataSetChanged();
    }

    public EntriesListViewAdapter(@NonNull Context context, int resource) {
        super(context, resource);
    }

    public EntriesListViewAdapter(@NonNull Context context, int resource, @NonNull List<EntryEntity> objects) {
        super(context, resource, objects);
    }

    /**
     * read preferences as Int
     * @param key
     * @return
     */
    private int readIntPreferences(String key) {
        final SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getContext());
        final int pos = prefs.getInt(key, -1);  // default value -1 if preferences not found
        return pos;
    }
}
