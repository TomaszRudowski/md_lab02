package no.ntnu.imt3673.tomaszmr.lab_02;

import android.app.job.JobInfo;
import android.app.job.JobScheduler;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.webkit.WebView;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import no.ntnu.imt3673.tomaszmr.lab_02.database.AppDatabase;
import no.ntnu.imt3673.tomaszmr.lab_02.database.ClearDatabaseAsyncTask;
import no.ntnu.imt3673.tomaszmr.lab_02.database.EntryEntity;
import no.ntnu.imt3673.tomaszmr.lab_02.database.FetchEntriesAsync;

public class MainActivity extends AppCompatActivity {

    private static EntriesListViewAdapter adapter;

    public static EntriesListViewAdapter getAdapter() {
        return adapter;
    }

    public static final int DEFAULT_NO_OF_ITEMS = 10;
    public static final int DEFAULT_FREQUENCY = 15; // because of minimum time requirements on JobSchedule
    static final int USER_PREF_REQUEST = 1;  // The request code
    // TODO: (?) move PREF_RSS_URL and PREF_NO_OF_ITEMS from const to strings.xml
    public static final String PREF_RSS_URL = "rssUrl";
    public static final String PREF_NO_OF_ITEMS = "noOfItems";
    public static final String PREF_FREQUENCY ="frequency";
    private static final int RSS_FETCH_JOB_ID = 12345;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        System.out.println("....................onCreate running...");
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        checkPreferences();
        int timeInterval = readIntPreferences(PREF_FREQUENCY) * 60000; // in miliseconds
        rescheduleRssFetch(timeInterval);
        // storePreferances(PREF_RSS_URL, "https://meta.stackexchange.com/feeds" );
        // "https://www.vg.no/rss/feed/?categories=&keywords=&limit=50&format=atom"


        List<EntryEntity> initEntry = new ArrayList<>();
        initEntry.add(new EntryEntity("test Title", "test Url"));

        final ListView lv_entries = (ListView) findViewById(R.id.list_entries);
        lv_entries.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int pos, long l) {
                EntryEntity selectedEntry = (EntryEntity) lv_entries.getItemAtPosition(pos);
                String entryUrlString = selectedEntry.getLink();
                Intent intent = new Intent(getApplicationContext(),ContentActivity.class);
                intent.putExtra("url", entryUrlString);
                intent.putExtra("ReloadView", true);
                startActivity(intent);
            }
        });

        this.adapter = new EntriesListViewAdapter(
                this,
                android.R.layout.simple_list_item_1,
                initEntry
        );
        lv_entries.setAdapter(adapter);

        new FetchEntriesAsync(getApplicationContext()).execute(adapter);
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        // TODO: reload ListView
        new FetchEntriesAsync(getApplicationContext()).execute(adapter);
        System.out.println("....................onRestart running...");
    }

    // https://medium.com/@magdamiu/android-room-persistence-library-97ad0d25668e
    @Override
    protected void onDestroy() {
        AppDatabase.destroyInstance();
        super.onDestroy();
    }

    /**
     * starts activity to alter store preferences
     * @param view
     */
    public void preferencesBtnOnClick (View view) {
        Intent intent = new Intent(this, UserPreferencesActivity.class);
        startActivityForResult(intent,USER_PREF_REQUEST);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == USER_PREF_REQUEST) {
            if (resultCode == RESULT_OK) {
                int previousNoOfItems = readIntPreferences(PREF_NO_OF_ITEMS);
                String previousUrl = readStringPreferences(PREF_RSS_URL);
                int previousFrequency = readIntPreferences(PREF_FREQUENCY);

                String newRssUrl = data.getStringExtra("newRssUrl");
                int newNoOfItems = data.getIntExtra("newNoOfItems", DEFAULT_NO_OF_ITEMS);
                int newFrequency = data.getIntExtra("newFrequency", DEFAULT_FREQUENCY);

                // reload list from database, no new http request
                if (newNoOfItems != previousNoOfItems) {
                    storePreferances(PREF_NO_OF_ITEMS, newNoOfItems);
                    new FetchEntriesAsync(getApplicationContext()).execute(this.adapter);
                }

                // new url, need to replace data with new rss entries from the new url
                // when done reload list view (ReloadView flag set til true)
                if (!newRssUrl.equals(previousUrl)) {
                    storePreferances(PREF_RSS_URL, newRssUrl);
                    Intent intent = new Intent(this, UpdateRssFeedService.class);
                    intent.putExtra("ReloadView", true);
                    startService(intent);
                }

                // when frequency changed, need to reschedule JobService
                if (newFrequency != previousFrequency) {
                    storePreferances(PREF_FREQUENCY, newFrequency);
                    int newInterval = newFrequency * 60000;
                    rescheduleRssFetch(newInterval);
                }

            }
            checkPreferences();
        }
    }

    /**
     * Help function for testing and forcing fetch of XML feed from given URL (SharedPreferences)
     * @param view
     */
    public void onForcedRssFetchBtnClick (View view) {
        String rssURL = readStringPreferences(PREF_RSS_URL);
        // TODO: call service UpdateRssFeedService, reload ListView
        if (rssURL == null || rssURL.isEmpty()) {
            // TODO: error Toast?
        } else {
            Intent intent = new Intent(this, UpdateRssFeedService.class);
            intent.putExtra("ReloadView", true);
            startService(intent);
        }
        }

    public void onClearDbBtnClick(View view) {
       new ClearDatabaseAsyncTask(getApplicationContext()).execute(adapter);
    }

    /**
     * version to store string (from imt3673 wiki)
     * @param key
     * @param value
     */
    private void storePreferances (String key, String value) {
        final SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
        final SharedPreferences.Editor editor = prefs.edit();
        editor.putString(key, value);
        editor.apply();
    }

    /**
     * version to store int
     * @param key
     * @param value
     */
    private void storePreferances (String key, int value) {
        final SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
        final SharedPreferences.Editor editor = prefs.edit();
        editor.putInt(key, value);
        editor.apply();
    }

    /**
     * read preferences as String (from imt3673 wiki)
     * @param key
     * @return
     */
    private String readStringPreferences(String key) {
        final SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
        final String value = prefs.getString(key, "");  // default value "" if preferences not found
        return value;
    }

    /**
     * read preferences as Int
     * @param key
     * @return
     */
    private int readIntPreferences(String key) {
        final SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
        final int pos = prefs.getInt(key, -1);  // default value -1 if preferences not found
        return pos;
    }

    private void checkPreferences() {
        Context context = getApplicationContext();
        String rssURL = readStringPreferences(PREF_RSS_URL);
        int noOfItems = readIntPreferences(PREF_NO_OF_ITEMS);
        int frequency = readIntPreferences(PREF_FREQUENCY);
        // if not exists set it to default value
        if (noOfItems < 0) {
            storePreferances(PREF_NO_OF_ITEMS, DEFAULT_NO_OF_ITEMS);
            noOfItems = DEFAULT_NO_OF_ITEMS;
        }
        CharSequence text = "Current preferences: url(" + rssURL +
                "), noOfItems(" + noOfItems + "), frequency (" + frequency + ")";
        int duration = Toast.LENGTH_SHORT;

        Toast toast = Toast.makeText(context, text, duration);
        toast.show();
    }

    private void rescheduleRssFetch(int timeInterval) {
        JobScheduler jobScheduler =
                (JobScheduler) getSystemService(Context.JOB_SCHEDULER_SERVICE);
        jobScheduler.schedule(new JobInfo.Builder(RSS_FETCH_JOB_ID,
                new ComponentName(this, UpdateRssFeedService.class))
                .setRequiredNetworkType(JobInfo.NETWORK_TYPE_ANY)
                .setPeriodic(timeInterval)
                .build());
        System.out.println("....................rescheduleRssFetch running...");
    }
}
