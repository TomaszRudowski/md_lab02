package no.ntnu.imt3673.tomaszmr.lab_02.database;

import android.content.Context;
import android.os.AsyncTask;

import java.util.ArrayList;
import java.util.List;

import no.ntnu.imt3673.tomaszmr.lab_02.EntriesListViewAdapter;

/**
 * Created by Reyn on 08-Mar-18.
 */

public class ClearDatabaseAsyncTask extends AsyncTask<EntriesListViewAdapter, Void, List<EntryEntity>> {

    private Context appContext;
    private EntriesListViewAdapter adapter;

    public ClearDatabaseAsyncTask(Context appContext) {
        this.appContext = appContext;
    }

    @Override
    protected List<EntryEntity> doInBackground(EntriesListViewAdapter... entriesListViewAdapters) {
        this.adapter = entriesListViewAdapters[0];
        AppDatabase.getAppDatabase(this.appContext).entryEntityDAO().delete();
        List<EntryEntity> lastEntries = AppDatabase.getAppDatabase(this.appContext).entryEntityDAO().getAll();
        return lastEntries;
    }

    @Override
    protected void onPostExecute(List<EntryEntity> lastEntries) {
        adapter.updateLastEntries(lastEntries);
    }
}
