package no.ntnu.imt3673.tomaszmr.lab_02.parser;

import android.util.Xml;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import no.ntnu.imt3673.tomaszmr.lab_02.database.EntryEntity;

/**
 * Using XmlPullParser,
 * Inspired by https://developer.android.com/training/basics/network-ops/xml.html
 * Version from tutorial should work with Atom format.
 * TODO: Create functions that could create Entry object based on tags used by RSS format
 * https://developer.android.com/reference/android/util/Xml.htm
 */

public class RssXmlParser {
    // We don't use namespaces
    private static final String ns = null;

    /**
     * From https://developer.android.com/training/basics/network-ops/xml.htm
     * @param in
     * @return
     * @throws XmlPullParserException
     * @throws IOException
     */
    public List parse(InputStream in) throws XmlPullParserException, IOException {
        try {
            XmlPullParser parser = Xml.newPullParser();
            parser.setFeature(XmlPullParser.FEATURE_PROCESS_NAMESPACES, false);
            parser.setInput(in, null);
            parser.nextTag();
            return readFeed(parser);
        } finally {
            in.close();
        }
    }

    /**
     * From https://developer.android.com/training/basics/network-ops/xml.htm
     * working with Atom only,
     * modified: TODO: add RSS handler
     * @param parser
     * @return
     * @throws XmlPullParserException
     * @throws IOException
     */
    private List readFeed(XmlPullParser parser) throws XmlPullParserException, IOException {
        List entries = new ArrayList();

        parser.require(XmlPullParser.START_TAG, ns, "feed");
        while (parser.next() != XmlPullParser.END_TAG) {
            if (parser.getEventType() != XmlPullParser.START_TAG) {
                continue;
            }
            String name = parser.getName();
            // Starts by looking for the entry tag
            if (name.equals("entry")) {
                entries.add(readAtomEntry(parser));
            } else if (name.equals("item")){
                //TODO: here handling RSS where tag is called 'item' instead of 'entry'
                //entries.add(readRssEntry(parser));
            }
            else {
                skip(parser);
            }
        }
        return entries;
    }



    /**
     * Parses the contents of an entry. If it encounters a title, summary, or link tag, hands
     * them off to their respective "read" methods for processing. Otherwise, skips the tag.
     * From https://developer.android.com/training/basics/network-ops/xml.htm
     * Here only title and link are used
     * @param parser
     * @return
     * @throws XmlPullParserException
     * @throws IOException
     */
    private EntryEntity readAtomEntry(XmlPullParser parser) throws XmlPullParserException, IOException {
        parser.require(XmlPullParser.START_TAG, ns, "entry");
        String title = null;
        String summary = null;
        String link = null;
        while (parser.next() != XmlPullParser.END_TAG) {
            if (parser.getEventType() != XmlPullParser.START_TAG) {
                continue;
            }
            String name = parser.getName();
            if (name.equals("title")) {
                title = readTitle(parser);
            } else if (name.equals("link")) {
                link = readAtomLink(parser);
            } else {
                skip(parser);
            }
        }
        return new EntryEntity(title, link);
    }

    //

    /**
     * Processes title tags in the feed.
     * From https://developer.android.com/training/basics/network-ops/xml.htm
     * This shoul works fine with both Atom and RSS
     * @param parser
     * @return
     * @throws IOException
     * @throws XmlPullParserException
     */
    private String readTitle(XmlPullParser parser) throws IOException, XmlPullParserException {
        parser.require(XmlPullParser.START_TAG, ns, "title");
        String title = readText(parser);
        parser.require(XmlPullParser.END_TAG, ns, "title");
        return title;
    }

    /**
     * Processes link tags in the feed.
     * From https://developer.android.com/training/basics/network-ops/xml.htm
     * TODO: this works with Atom only, create another for RSS
     * @param parser
     * @return
     * @throws IOException
     * @throws XmlPullParserException
     */
    private String readAtomLink(XmlPullParser parser) throws IOException, XmlPullParserException {
        String link = "";
        parser.require(XmlPullParser.START_TAG, ns, "link");
        String tag = parser.getName();
        String relType = parser.getAttributeValue(null, "rel");
        if (tag.equals("link")) {
            if (relType.equals("alternate")){
                link = parser.getAttributeValue(null, "href");
                parser.nextTag();
            }
        }
        parser.require(XmlPullParser.END_TAG, ns, "link");
        return link;
    }

    /**
     * From https://developer.android.com/training/basics/network-ops/xml.htm
     * This should work fine with all tags that have text values
     * @param parser
     * @return
     * @throws IOException
     * @throws XmlPullParserException
     */
    private String readText(XmlPullParser parser) throws IOException, XmlPullParserException {
        String result = "";
        if (parser.next() == XmlPullParser.TEXT) {
            result = parser.getText();
            parser.nextTag();
        }
        return result;
    }

    /**
     * Ignore tags not considered currently
     * From https://developer.android.com/training/basics/network-ops/xml.htm
     * @param parser
     * @throws XmlPullParserException
     * @throws IOException
     */
    private void skip(XmlPullParser parser) throws XmlPullParserException, IOException {
        if (parser.getEventType() != XmlPullParser.START_TAG) {
            throw new IllegalStateException();
        }
        int depth = 1;
        while (depth != 0) {
            switch (parser.next()) {
                case XmlPullParser.END_TAG:
                    depth--;
                    break;
                case XmlPullParser.START_TAG:
                    depth++;
                    break;
            }
        }
    }


}
